package com.epam.exception;

public class MailNotSentException extends Exception{
	
	public MailNotSentException(String message) {
		super(message);
	}

}
