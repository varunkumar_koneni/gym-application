package com.epam.exception;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import com.epam.model.ExceptionResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;


@RestControllerAdvice
@Slf4j
public class RestExceptionHandler {
	


	@ExceptionHandler(MailNotSentException.class)
	public ResponseEntity<Object> handleMailException(MailNotSentException ex, WebRequest request) {
	    ExceptionResponse exceptionResponse = new ExceptionResponse(new Date().toString(),
	    		HttpStatus.BAD_REQUEST.name(), ex.getMessage(), request.getDescription(false));
	    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exceptionResponse);
	}


	

}
