package com.epam.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.epam.exception.MailNotSentException;
import com.epam.model.EmailRequest;
import com.epam.service.EmailService;
//import org.springframework.kafka.support.Acknowledgment;


import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class EmailController{
	
	@Autowired
    public EmailService emailService;
		

    @KafkaListener(topics = "notification3", groupId = "email-service-group")
    public void receiveEmailRequest(EmailRequest emailRequest) throws MailNotSentException {
        log.info("Received email request from Kafka for: {}", emailRequest.getTo());
        emailService.sendTextMail(emailRequest);
    }
	
    
  
    
	

}
