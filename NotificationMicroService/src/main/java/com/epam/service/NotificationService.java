package com.epam.service;

import com.epam.exception.MailNotSentException;
import com.epam.model.EmailRequest;

public interface NotificationService {
    public void sendTextMail(EmailRequest emailRequest) throws MailNotSentException ;


}
