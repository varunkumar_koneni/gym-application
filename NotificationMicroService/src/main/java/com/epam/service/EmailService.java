package com.epam.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.epam.exception.MailNotSentException;
import com.epam.model.Email;
import com.epam.model.EmailRequest;
import com.epam.repository.EmailRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class EmailService implements NotificationService {

    @Autowired
    EmailRepository emailRepository;

    @Autowired
    JavaMailSender javaMailSender;

    @Autowired
    Email email;

    @Override
    public void sendTextMail(EmailRequest emailRequest) throws MailNotSentException {
        log.info("Sending email to: {}", emailRequest.getTo());

        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(emailRequest.getTo());
        message.setSubject(emailRequest.getSubject());
        message.setText(emailRequest.getBody());

        try {
            javaMailSender.send(message);
            email.setStatus("SUCCESS");
            email.setRemarks("Mail Sent Successfully");
            log.info("Email sent successfully to: {}", emailRequest.getTo());
        } catch (Exception e) {
            email.setStatus("FAILURE");
            email.setRemarks(e.getMessage());
            log.error("Failed to send email to: {}", emailRequest.getTo(), e);
        }

        email.setTo(emailRequest.getTo());
        email.setSubject(emailRequest.getSubject());
        email.setBody(emailRequest.getBody());
        emailRepository.save(email);

        log.info("Email details saved in the repository for: {}", emailRequest.getTo());
    }
}
