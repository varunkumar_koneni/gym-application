package com.epam.model;

import org.springframework.data.annotation.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class EmailRequest {
	
	@Id
	private int id;
    private String to;
    private String subject;
    private String body;
     
	

    
}
