package com.epam;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import com.epam.exception.MailNotSentException;
import com.epam.model.Email;
import com.epam.model.EmailRequest;
import com.epam.repository.EmailRepository;
import com.epam.service.EmailService;

@ExtendWith(MockitoExtension.class)
public class EmailServiceTesting {
	
    @Mock
    private EmailRepository emailRepository;

    @Mock
    private JavaMailSender javaMailSender;

    @Mock
    private Email email;

    @InjectMocks
    private EmailService emailService;
    
    @Test
    public void testSendTextMail_Success() throws MailNotSentException {
        EmailRequest emailRequest =EmailRequest.builder()
        		.to("varun@gmail.com")
        		.subject("HI")
        		.body("bye")
        		.build();

        doNothing().when(javaMailSender).send(Mockito.any(SimpleMailMessage.class));
        when(emailRepository.save(Mockito.any(Email.class))).thenReturn(new Email());

        emailService.sendTextMail(emailRequest);

        verify(emailRepository).save(Mockito.any(Email.class));


    }
    
    @Test
    public void testSendTextMail_Failure() throws MailNotSentException {
        EmailRequest emailRequest =EmailRequest.builder()
        		.to("varun@gmail.com")
        		.subject("HI")
        		.body("bye")
        		.build();

        doThrow(new RuntimeException("Mail sending failed")).when(javaMailSender).send(Mockito.any(SimpleMailMessage.class));
        when(emailRepository.save(Mockito.any(Email.class))).thenReturn(new Email());

        emailService.sendTextMail(emailRequest);

        verify(emailRepository).save(Mockito.any(Email.class));


    }

}
