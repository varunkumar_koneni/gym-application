package com.epam;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.security.Key;

import javax.crypto.SecretKey;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import com.epam.service.JwtService;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;

@ExtendWith(MockitoExtension.class)
public class JwtServiceTest {

	@InjectMocks
	private JwtService jwtService;

	@Mock
	private Keys keys;

	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testGenerateToken() {
		String username = "testUser";

		String token = jwtService.generateToken(username);

		assertNotNull(token);
	}

//	@Test
//	public void testValidateToken() {
//		String mockToken = "valid.jwt.token"; // Replace with an actual valid JWT token
//
//		Key signKey = Keys.hmacShaKeyFor(new byte[64]);
//		Jws<Claims> jws = Jwts.parserBuilder().setSigningKey(signKey).build().parseClaimsJws(mockToken);
//
//		when(keys.hmacShaKeyFor(any(byte[].class))).thenReturn((SecretKey) signKey);
//
//		assertDoesNotThrow(() -> jwtService.validateToken(mockToken));
//
//		verify(keys, times(1)).hmacShaKeyFor(any(byte[].class));
//		verifyNoMoreInteractions(keys);
//	}
}