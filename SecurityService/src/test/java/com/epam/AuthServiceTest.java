package com.epam;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.epam.repository.UserRepository;
import com.epam.service.AuthService;
import com.epam.service.JwtService;


@ExtendWith(MockitoExtension.class)
class AuthServiceTest {
	@Mock
	private UserRepository repository;
	@Mock
	private PasswordEncoder passwordEncoder;

	@Mock
	private JwtService jwtService;

	@InjectMocks
	private AuthService authService;


	@Test
	void testGenerateToken() {
		when(jwtService.generateToken(anyString())).thenReturn("asdfr");
		assertNotNull(authService.generateToken("sai"));
	}

	@Test
	void testValidateToken() {
		authService.validateToken("sai");
		Mockito.verify(jwtService).validateToken(anyString());
	}

}
