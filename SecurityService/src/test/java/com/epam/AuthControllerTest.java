package com.epam;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.epam.controller.AuthController;
import com.epam.dto.AuthRequest;
import com.epam.service.AuthService;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(controllers = AuthController.class)
class AuthControllerTest {

	@MockBean
	private AuthService service;

	@MockBean
	private AuthenticationManager authenticationManager;

	@Autowired
	private MockMvc mockMvc;
	@MockBean
	Authentication authenticate;

	@WithMockUser(value = "konenivarun123@gmail.com", password = "java.util.stream.DoublePipeline$Head@44eef651")
	@Test
	void testGetToken() throws Exception {
		AuthRequest authRequest = new AuthRequest("konenivarun123@gmail.com",
				"java.util.stream.DoublePipeline$Head@44eef651");
		String input = "hello";
		Mockito.when(authenticationManager.authenticate(any())).thenReturn(authenticate);
		Mockito.when(authenticationManager.authenticate(any()).isAuthenticated()).thenReturn(true);
		Mockito.when(service.generateToken(any())).thenReturn(input);
		MvcResult mvcResult = mockMvc
				.perform(post("/auth/token").content(new ObjectMapper().writeValueAsString(authRequest))
						.contentType(MediaType.APPLICATION_JSON).with(csrf()))
				.andReturn();

		assertEquals(input, mvcResult.getResponse().getContentAsString());
		assertNotNull(mvcResult);

	}

	
	@WithMockUser(value = "konenivarun123@gmail.com", password = "java.util.stream.DoublePipeline$Head@44eef651")
	@Test
	void testGetTokenUnsucessfull() throws Exception {
		AuthRequest authRequest = new AuthRequest("konenivarun123@gmail.com",
				"java.util.stream.DoublePipeline$Head@44eef651");

		Mockito.when(authenticationManager.authenticate(any())).thenReturn(authenticate);
		Mockito.when(authenticationManager.authenticate(any()).isAuthenticated()).thenReturn(false);

		MvcResult mvcResult = mockMvc
				.perform(post("/auth/token").content(new ObjectMapper().writeValueAsString(authRequest))
						.contentType(MediaType.APPLICATION_JSON).with(csrf()))
				.andReturn();

		assertEquals("invalid access", mvcResult.getResponse().getContentAsString());
		assertNotNull(mvcResult);

	}
	
	


	@WithMockUser(value = "konenivarun123@gmail.com", password = "java.util.stream.DoublePipeline$Head@44eef651")
	@Test
	void testvalidateToken() throws Exception {

		MvcResult mvcResult = mockMvc.perform(get("/auth/validate").param("token", "123")).andReturn();

		String result = mvcResult.getResponse().getContentAsString();

		assertEquals("Token is valid", result);

	}

}
