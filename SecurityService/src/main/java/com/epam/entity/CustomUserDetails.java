package com.epam.entity;

import java.time.Instant;
import java.util.Collection;
import java.util.Collections;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;


public class CustomUserDetails implements UserDetails {

	private static final long serialVersionUID = 1L;
	private String username;
    private String password;
    private int failedLoginAttempts;
    private Instant lastFailedLoginTime;

    public CustomUserDetails(User user) {
        this.username = user.getUsername();
        this.password = user.getPassword();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.emptyList();
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

//    @Override
//    public boolean isAccountNonLocked() {
//        return true;
//    }
    @Override
    public boolean isAccountNonLocked() {
        return failedLoginAttempts < 3 || !isLockedOut();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
    
    public boolean isLockedOut() {
        int maxFailedAttempts = 3;
        long lockoutDurationMinutes = 5;
        Instant lockoutEndTime = lastFailedLoginTime.plusSeconds(lockoutDurationMinutes * 60);

        return failedLoginAttempts >= maxFailedAttempts && Instant.now().isBefore(lockoutEndTime);
    }
    
    
}
