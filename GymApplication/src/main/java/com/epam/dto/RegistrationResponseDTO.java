package com.epam.dto;

import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Component
@Setter
@Getter
public class RegistrationResponseDTO {
	
    private String username;
    private String password;    

}
