package com.epam.dto;

import org.springframework.stereotype.Service;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TrainingTypeDTO {
	
    private int id;
    private String trainingTypeName;

}
