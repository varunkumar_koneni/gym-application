package com.epam.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TrainerRegistrationRequestDTO {

    @NotBlank(message = "First name must not be blank")
    @Size(max = 20, message = "First name must be at most 20 characters long")
    private String firstName;

    @NotBlank(message = "Last name must not be blank")
    @Size(max = 20, message = "Last name must be at most 20 characters long")
    private String lastName;

    @NotBlank(message = "Specialization must not be blank")
    @Size(max = 100, message = "Specialization must be at most 100 characters long")
    private String specialization;

    @NotBlank(message = "Email must not be blank")
    @Email(message = "Email should be valid")
    private String email;

}
