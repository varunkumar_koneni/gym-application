package com.epam.dto;


import com.epam.model.TrainingType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UpdateTrainerProfileDTO {
	
	private String username;
    private String firstName;
    private String lastName;
    private TrainingType specialization;
    private boolean isActive;
	
}
