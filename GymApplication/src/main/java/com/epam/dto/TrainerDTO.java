package com.epam.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TrainerDTO {
	
    private String username;
    private String firstName;
    private String lastName;
    private String specialization;
    
}
