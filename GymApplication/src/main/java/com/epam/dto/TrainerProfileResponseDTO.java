package com.epam.dto;

import java.util.List;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class TrainerProfileResponseDTO {
	private String firstName;
    private String lastName;
    private String specialization;
    private boolean isActive;
    private List<TraineeDTO> trainees;

}
