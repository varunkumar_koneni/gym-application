package com.epam.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.epam.model.Trainee;

@Repository
public interface TraineeRepository extends JpaRepository<Trainee, Integer>{

	Trainee findByUserId(Long id);

	Trainee findByUserUsername(String traineeUsername);



}
