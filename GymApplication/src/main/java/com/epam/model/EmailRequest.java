package com.epam.model;

import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Component
public class EmailRequest {
    private String to;
    private String subject;
    private String body;

}
