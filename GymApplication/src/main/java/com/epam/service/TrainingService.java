package com.epam.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import com.epam.dto.AddTrainingRequestDTO;
import com.epam.dto.TrainerReportDto;
import com.epam.exception.TrainerNotFoundException;
import com.epam.model.EmailRequest;
import com.epam.model.Trainee;
import com.epam.model.Trainer;
import com.epam.model.Training;
import com.epam.model.TrainingType;
import com.epam.model.User;
import com.epam.repository.TraineeRepository;
import com.epam.repository.TrainerRepository;
import com.epam.repository.TrainingRepository;
import com.epam.repository.TrainingTypeRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class TrainingService {

	
	@Autowired
	TraineeRepository traineeRepository;
	
	@Autowired
	TrainerRepository trainerRepository;
	
	@Autowired
	TrainingRepository trainingRepository;
	
	
	@Autowired
	TrainingTypeRepository trainingTypeRepository;
	
	@Autowired
	Training training;
	
	@Autowired
	EmailService emailService;
	

    @Autowired
    private KafkaTemplate<String, EmailRequest> kafkaReportTemplate;

    private static final String REPORT_TOPIC = "report";

	public void addTraining(AddTrainingRequestDTO request) throws TrainerNotFoundException {
        log.info("Received a request to add training: Trainee - {}, Trainer - {}, Training - {}",
                request.getTraineeUsername(), request.getTrainerUsername(), request.getTrainingName());
		
		 Trainee trainee = traineeRepository.findByUserUsername(request.getTraineeUsername());
	     Trainer trainer = trainerRepository.findByUserUsername(request.getTrainerUsername());
	     
	     trainee.getTrainerList().add(trainer);
	     traineeRepository.save(trainee);

	     //TrainingType trainingType = trainingTypeRepository.findByTrainingTypeName(request.getTrainingName());
	     training.setTrainee(trainee);
	     training.setTrainer(trainer);
	     training.setTrainingName(request.getTrainingName());
	     training.setTrainingDate(request.getTrainingDate());
	     training.setTrainingType(trainer.getSpecialization());
	     training.setTrainingDuration(request.getTrainingDuration());
	     trainingRepository.save(training);
	     emailService.sendTrainingConfirmationEmailToTrainee(trainee.getUser().getEmail(),training);
	     emailService.sendTrainingConfirmationEmailToTrainer(trainer.getUser().getEmail(),training);
	     
	     TrainerReportDto trainerReportDto= TrainerReportDto.builder().trainerUsername(trainer.getUser().getUsername())
	     							.firstName(trainer.getUser().getFirstName())
	     							.lastName(trainer.getUser().getLastName())
	     							.isActive(trainer.getUser().isActive())
	     							.trainingDate(request.getTrainingDate())
	     							.trainingDuration(request.getTrainingDuration()).build();
	     
	        Message<TrainerReportDto> report = MessageBuilder.withPayload(trainerReportDto)
	                .setHeader(KafkaHeaders.TOPIC, REPORT_TOPIC).build();

	        kafkaReportTemplate.send(report);
	     									
	}

}
