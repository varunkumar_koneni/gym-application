package com.epam.service;

import com.epam.dto.TrainerReportDto;


public interface TrainerReportService {
    void addTrainerReport(TrainerReportDto trainerReportDto);
}
