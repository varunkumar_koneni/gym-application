package com.epam.service;

import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.collections.MonthData;
import com.epam.collections.TrainerReport;
import com.epam.collections.YearData;
import com.epam.dto.TrainerReportDto;
import com.epam.repositories.TrainerReportRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class TrainerReportServiceImpl implements TrainerReportService{

    @Autowired
    private TrainerReportRepository trainerReportRepository;

    public void addTrainerReport(TrainerReportDto trainerReportDto){
        Optional<TrainerReport> trainerReport = trainerReportRepository.findTrainerReportByUsername(trainerReportDto.getTrainerUsername());
        if(trainerReport.isPresent()) {
            trainerReportExist(trainerReportDto,trainerReport.get());
        }
        else {
            trainerReportNotExist(trainerReportDto);
        }
    }

    private TrainerReport trainerReportNotExist(TrainerReportDto trainerReportDto)
    {
        int trainingDurationSummary = trainerReportDto.getTrainingDuration();
        int year = trainerReportDto.getTrainingDate().getYear();
        Month month = trainerReportDto.getTrainingDate().getMonth();
        TrainerReport newTrainerReport = TrainerReport.builder()
                .username(trainerReportDto.getTrainerUsername())
                .firstName(trainerReportDto.getFirstName())
                .lastName(trainerReportDto.getLastName())
                .isActive(trainerReportDto.isActive())
                .build();
        MonthData monthData = new MonthData(month,trainingDurationSummary);
        YearData yearData = new YearData(year,List.of(monthData));
        newTrainerReport.setYearsList(List.of(yearData));
        newTrainerReport = trainerReportRepository.save(newTrainerReport);
        log.info("New Trainer Report: {} ",newTrainerReport);
        return newTrainerReport;
    }
    private TrainerReport trainerReportExist(TrainerReportDto trainerReportDto, TrainerReport trainerReport) {
        int trainingDurationSummary = trainerReportDto.getTrainingDuration();
        int year = trainerReportDto.getTrainingDate().getYear();
        Month month = trainerReportDto.getTrainingDate().getMonth();

        boolean yearDataFound = false;
        for (YearData yearData : trainerReport.getYearsList()) {
            if (yearData.getYear() == year) {
                boolean monthDataFound = false;
                for (MonthData monthData : yearData.getMonthsList()) {
                    if (monthData.getMonth() == month) {
                        monthData.setTrainingDurationSummary(monthData.getTrainingDurationSummary() + trainingDurationSummary);
                        monthDataFound = true;
                        break;
                    }
                }
                if (!monthDataFound) {
                    yearData.getMonthsList().add(new MonthData(month, trainingDurationSummary));
                }
                yearDataFound = true;
                break;
            }
        }

        if (!yearDataFound) {
            List<MonthData> newMonthList = new ArrayList<>();
            newMonthList.add(new MonthData(month, trainingDurationSummary));
            trainerReport.getYearsList().add(new YearData(year, newMonthList));
        }

        log.info("Trainer Report: {} ", trainerReport);
        return trainerReport;
    }



}
