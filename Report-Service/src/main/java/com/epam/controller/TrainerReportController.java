package com.epam.controller;

import com.epam.dto.TrainerReportDto;
import com.epam.service.TrainerReportService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("reports")
@Slf4j
public class TrainerReportController {
    @Autowired
    private TrainerReportService trainerReportService;

    @KafkaListener(topics = "report", groupId = "report-service-group")
    public void addTrainerReport(@RequestBody @Valid TrainerReportDto trainerReportDto){
        log.info("TrainerReportController:addTrainerReport execution started.");
        log.info("TrainerReportDto: {}",trainerReportDto);
        trainerReportService.addTrainerReport(trainerReportDto);
    }
}
