package com.epam.filter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;

import com.epam.proxy.WebFluxAuthenticationProxy;
import com.epam.util.JwtUtil;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Component
@Slf4j
public class AuthenticationFilter extends AbstractGatewayFilterFactory<AuthenticationFilter.Config> {

	private final RouteValidator routeValidator;
	private final WebFluxAuthenticationProxy authenticationProxy;

	@Autowired
	JwtUtil jwtUtil;

	public AuthenticationFilter(RouteValidator routeValidator, WebFluxAuthenticationProxy authenticationProxy) {
		super(Config.class);
		this.routeValidator = routeValidator;
		this.authenticationProxy = authenticationProxy;
	}

	@Override
	public GatewayFilter apply(Config config) {
		return ((exchange, chain) -> {
			if (routeValidator.isSecured.test(exchange.getRequest())) {
				if (!exchange.getRequest().getHeaders().containsKey(HttpHeaders.AUTHORIZATION)) {
					throw new RuntimeException("missing authorization header");
				}

				String authHeader = exchange.getRequest().getHeaders().get(HttpHeaders.AUTHORIZATION).get(0);
				if (authHeader != null && authHeader.startsWith("Bearer ")) {
					authHeader = authHeader.substring(7);
				}
				try {
					// jwtUtil.validateToken(authHeader);
					log.info("Received token : " + authHeader);

					return validateToken(authHeader).flatMap(valid -> {
						if (valid) {
							log.info("Valid token received");
							return chain.filter(exchange);
						} else {
							log.info("Token validation failed");
							ServerHttpResponse response = exchange.getResponse();
							response.setStatusCode(HttpStatus.UNAUTHORIZED);
							return response.setComplete();
						}
					});

				} catch (Exception e) {

					throw new RuntimeException("un authorized access to application");
				}
			}
			return chain.filter(exchange);
		});
	}

	private Mono<Boolean> validateToken(String token) {
		log.info("Validating token : " + token);
		return authenticationProxy.validateToken(token).map(response -> response.equals("Token is valid")); 
	}

	public static class Config {

	}
}
